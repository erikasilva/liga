import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LigaModule } from './liga/liga.module';

@Module({
  imports: [LigaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
