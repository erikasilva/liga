import {Controller, Get, Post, Res, Body,Param, Req} from "@nestjs/common";
import {LigaService} from "./liga.service";
import {Jugador} from "./interfaces/jugador";
import {Arbitro} from "./interfaces/arbitro";
import {Equipo} from "./interfaces/equipo";

@Controller('/liga')
export class LigaController {

    constructor(private readonly _ligaService: LigaService) {

    }

    @Get('ligaDeportiva')
    getModulos(
        @Res() res
    ){
        res.render('principal/vista-modulos', {
        })
    }

    @Get('contacto')
    getContacto(
        @Res() res
    ){
        res.render('principal/contacto', {
        })
    }

    /******* EQUIPOS *******/

    @Get('listarEquipos')
    listarEquipos(
        @Res() res
    ) {
        const arregloEquipos = this._ligaService.bddEquipos;
        res.render('equipos/lista-equipos', {
            arregloEquipos: arregloEquipos
        })
    }

    @Get('crearEquipo')
    crearEquipo(
        @Res() res
    ) {
        res.render('equipos/crear-equipo')
    }

    @Post('crearEquipo')
    crearEquipoPost(
        @Body() equipo:Equipo,
        @Res() res,
    ){
        this._ligaService.crearEquipo(equipo);
        res.redirect('/liga/listarEquipos');

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }

    }

    @Get('editarEquipo/:id')
     actualizarEquipoVista(
        @Res() response,
        @Param('id') idEquipo: string,
    ) {
        const equipoEncontrado = this._ligaService.buscarEquipoPorId(+idEquipo);

        response
            .render(
                'equipos/editar-equipo',
                {
                    equipo: equipoEncontrado
                }
            )
    }

    @Post('editarEquipo/:id')
    actualizarEquipo(
        @Res() res,
        @Param('id') idEquipo:String,
        @Body() equipo: Equipo
        )
    {
        equipo.id=+idEquipo;
        this._ligaService.actualizarEquipo(equipo,+idEquipo);
        res.redirect('/liga/listarEquipos');
    }

    @Post('eliminarEquipo')
    eliminarEquipo(@Res() res,
                  @Body('id') id: string) {

        this._ligaService.eliminarEquipoPorId(Number(id));
        res.redirect('/liga/listarEquipos');
    }

    @Post('buscarEquipo')
    buscarEquipo(
        @Res() res,
        @Req() req,
        @Body('busqueda') busqueda:string
    ){
        const listaBusqueda:Equipo[]=this._ligaService.buscarEquipoPorNombre(busqueda);
        if(listaBusqueda != null){
            res.render('equipos/lista-equipos',{arregloEquipos:listaBusqueda})
        }else{
            res.redirect('/liga/listarEquipos');
        }
    }

    /******* JUGADORES *******/

    @Get('listarJugadores')
    listarJugadores(
        @Res() res
    ) {
        const arregloJugadores = this._ligaService.bddJugadores;
        res.render('jugadores/lista-jugadores', {
            arregloJugadores: arregloJugadores
        })
    }

    @Get('crearJugador')
    crearJugador(
        @Res() res
    ) {
        res.render('jugadores/crear-jugador')
    }

    @Post('crearJugador')
    crearJugadorPost(
        @Body() jugador:Jugador,
        @Res() res,
    ){
        jugador.numeroCamiseta = Number(jugador.numeroCamiseta);
        this._ligaService.crearJugador(jugador);
        res.redirect('/liga/listarJugadores');
    }

    @Get('editarJugador/:id')
     actualizarJugadorVista(
        @Res() response,
        @Param('id') idJugador: string,
    ) {
        const jugadorEncontrado = this._ligaService.buscarJugadorPorId(+idJugador);
        response
            .render(
                'jugadores/editar-jugador',
                {
                    jugador: jugadorEncontrado
                }
            )
    }

    @Post('editarJugador/:id')
    actualizarJugador(
        @Res() res,
        @Param('id') idJugador:String,
        @Body() jugador: Jugador
        )
    {
        jugador.id=+idJugador;
        this._ligaService.actualizarJugador(jugador,+idJugador);
        res.redirect('/liga/listarJugadores');
    }

    @Post('eliminarJugador')
    eliminarJugador(@Res() res,
                  @Body('id') id: string) {
        this._ligaService.eliminarJugadorPorId(Number(id));
        res.redirect('/liga/listarJugadores');
    }

    @Post('buscarJugador')
    buscarJugador(
        @Res() res,
        @Req() req,
        @Body('busqueda') busqueda:string
    ){
        const listaBusqueda:Jugador[]=this._ligaService.buscarJugadorPorNombre(busqueda);
        if(listaBusqueda != null){
            res.render('jugadores/lista-jugadores',{arregloJugadores:listaBusqueda})
        }else{
            res.redirect('/liga/listarJugadores');
        }
    }
    
    /******* ARBITROS *******/

    @Get('listarArbitros')
    listarArbitros(
        @Res() res
    ) {
        const arregloArbitros = this._ligaService.bddArbitros;
        res.render('arbitros/lista-arbitros', {
            arregloArbitros: arregloArbitros
        })
    }

    @Get('crearArbitro')
    crearArbitro(
        @Res() res
    ) {
        res.render('arbitros/crear-arbitro')
    }

    @Post('crearArbitro')
    crearArbitroPost(
        @Body() arbitro:Arbitro,
        @Res() res,
    ){
        this._ligaService.crearArbitro(arbitro);
        res.redirect('/liga/listarArbitros');
    }

    @Get('editarArbitro/:id')
     actualizarArbitroVista(
        @Res() response,
        @Param('id') idArbitro: string,
    ) {
        const arbitroEncontrado = this._ligaService.buscarArbitroPorId(+idArbitro);
        response
            .render(
                'arbitros/editar-arbitro',
                {
                    arbitro: arbitroEncontrado
                }
            )
    }

    @Post('editarArbitro/:id')
    actualizarArbitro(
        @Res() res,
        @Param('id') idArbitro:String,
        @Body() arbitro: Arbitro
        )
    {
        console.log(arbitro);
        arbitro.id=+idArbitro;
        this._ligaService.actualizarArbitro(arbitro,+idArbitro);
        res.redirect('/liga/listarArbitros');
    }

    @Post('eliminarArbitro')
    eliminarArbitro(@Res() res,
                  @Body('id') id: string) {

        this._ligaService.eliminarArbitroPorId(Number(id));
        res.redirect('/liga/listarArbitros');
    }

    @Post('buscarArbitro')
    buscarArbitro(
        @Res() res,
        @Req() req,
        @Body('busqueda') busqueda:string
    ){
        const listaBusqueda:Arbitro[]=this._ligaService.buscarArbitroPorNombre(busqueda);
        console.log(listaBusqueda);
        console.log(busqueda);
        if(listaBusqueda != null){
            res.render('arbitros/lista-arbitros',{arregloArbitros:listaBusqueda})
        }else{
            res.redirect('/liga/lista-arbitros');
        }
    }

    


}