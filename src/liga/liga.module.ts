import {Module} from "@nestjs/common";
import {LigaController} from "./liga.controller";
import {LigaService} from "./liga.service";

@Module({
    imports:[],  // Modulos
    controllers:[
        LigaController
    ], // Controladores
    providers:[
        LigaService
    ], // Servicios
    exports:[
        LigaService
    ] // Exportar Servicios
})
export class LigaModule {
}