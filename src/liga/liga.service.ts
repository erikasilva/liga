import {Injectable} from "@nestjs/common";
import {Equipo} from "./interfaces/equipo";
import {Jugador} from "./interfaces/jugador";
import {Arbitro} from "./interfaces/arbitro";

@Injectable()
export class LigaService {

    bddJugadores = [];
    bddEquipos = [];
    bddArbitros = [];
    recnum = 1;

    constructor(){
        const equipo = {
            nombrePresidente: 'Nombre 1',
            apellidoPresidente: 'Apellido 1',
            nombreEquipo: 'Equipo 1',
            fechaCreacion: new Date(2019,5,10),
            colores: 'Azul, Verde'
        };
        this.listarEquipo(equipo);
        
        const jugador = {
            nombres: 'Nombre 1',
            apellidos: 'Apellido 1',
            numeroCamiseta: 1,
            posicion: 'Arquero',
            nombreEquipo: 'Equipo 1'
        };
        this.listarJugador(jugador);

        const arbitro = {
            nombres: 'Nombre 1',
            apellidos: 'Apellido 1',
            tipo: 'Central',
        };
        this.listarArbitro(arbitro);
    }

    /******* EQUIPOS *******/
    listarEquipo(nuevoEquipo) {
        nuevoEquipo.id = this.recnum;
        this.recnum++;
        this.bddEquipos.push(nuevoEquipo);
        return nuevoEquipo;
    }

    crearEquipo(nuevoEquipo: Equipo):Equipo {
        nuevoEquipo.id = this.recnum;
        this.recnum++;
        this.bddEquipos.push(nuevoEquipo);
        return nuevoEquipo;
    }

    buscarEquipoPorId(id: number):Equipo {
        return this.bddEquipos.find(
            (equipo) => {
                return equipo.id === id;
            }
        );
    }

    buscarEquipoPorNombre(nombre: string):Equipo[] {
        if(nombre!=='' && nombre!==null){
            return this.bddEquipos.filter(
            (equipo) => {
                console.log(equipo.nombreEquipo);
                console.log(nombre);
                return equipo.nombreEquipo.toUpperCase().includes(nombre.toUpperCase());
            }
        );
    }else{
            return this.bddEquipos;
        }
    }

    eliminarEquipoPorId(id: number):Equipo[] {
        console.log('id',id)
        const indice = this.bddEquipos.findIndex(
            (equipo) => {
                return equipo.id === id
            }
        );
        this.bddEquipos.splice(indice,1);
        return this.bddEquipos;
    }

    actualizarEquipo(equipoActualizado: Equipo, id:number):Equipo[] {

        const indice = this.bddEquipos.findIndex(
            (equipo) => {
                return equipo.id === id
            }
        );
        console.log(indice);
        equipoActualizado.id = this.bddEquipos[indice].id;
        this.bddEquipos[indice] = equipoActualizado;
        return this.bddEquipos;
    }

     /******* JUGADORES *******/

    listarJugador(nuevoJugador) {
        nuevoJugador.id = this.recnum;
        this.recnum++;
        this.bddJugadores.push(nuevoJugador);
        return nuevoJugador;
    }

    crearJugador(nuevoJugador: Jugador):Jugador {
        nuevoJugador.id = this.recnum;
        this.recnum++;
        this.bddJugadores.push(nuevoJugador);
        return nuevoJugador;
    }

    buscarJugadorPorId(id: number):Jugador {
        return this.bddJugadores.find(
            (jugador) => {
                return jugador.id === id;
            }
        );
    }

    buscarJugadorPorNombre(nombre: string):Jugador[] {
        if(nombre!=='' && nombre!==null){
            return this.bddJugadores.filter(
            (jugador) => {
                return jugador.nombres.toUpperCase().includes(nombre.toUpperCase());
            }
        );
            console.log(nombre);
    }else{
            return this.bddJugadores;
        }
    }

    eliminarJugadorPorId(id: number):Jugador[] {
        console.log('id',id)
        const indice = this.bddJugadores.findIndex(
            (jugador) => {
                return jugador.id === id
            }
        );
        this.bddJugadores.splice(indice,1);
        return this.bddJugadores;
    }

    actualizarJugador(jugadorActualizado: Jugador, id:number):Jugador[] {

        const indice = this.bddJugadores.findIndex(
            (jugador) => {
                return jugador.id === id
            }
        );
        console.log(indice);
        jugadorActualizado.id = this.bddJugadores[indice].id;
        this.bddJugadores[indice] = jugadorActualizado;
        return this.bddJugadores;
    }


    
     /******* ARBITROS *******/

     listarArbitro(nuevoArbitro) {
        nuevoArbitro.id = this.recnum;
        this.recnum++;
        this.bddArbitros.push(nuevoArbitro);
        return nuevoArbitro;
    }

    crearArbitro(nuevoArbitro: Arbitro):Arbitro {
        nuevoArbitro.id = this.recnum;
        this.recnum++;
        this.bddArbitros.push(nuevoArbitro);
        return nuevoArbitro;
    }

    buscarArbitroPorId(id: number):Arbitro {
        return this.bddArbitros.find(
            (arbitro) => {
                return arbitro.id === id;
            }
        );
    }

    buscarArbitroPorNombre(nombre: string):Arbitro[] {
        if(nombre!=='' && nombre!==null){
            return this.bddArbitros.filter(
            (arbitro) => {
                return arbitro.nombres.toUpperCase().includes(nombre.toUpperCase());
            }
        );
            console.log(nombre);
    }else{
            return this.bddArbitros;
        }
    }

    eliminarArbitroPorId(id: number):Arbitro[] {
        console.log('id',id)
        const indice = this.bddArbitros.findIndex(
            (arbitro) => {
                return arbitro.id === id
            }
        );
        this.bddArbitros.splice(indice,1);
        return this.bddArbitros;
    }

    actualizarArbitro(arbitroActualizado: Arbitro, id:number):Arbitro[] {

        const indice = this.bddArbitros.findIndex(
            (arbitro) => {
                return arbitro.id === id
            }
        );
        console.log(indice);
        arbitroActualizado.id = this.bddArbitros[indice].id;
        this.bddArbitros[indice] = arbitroActualizado;
        return this.bddArbitros;
    }
}