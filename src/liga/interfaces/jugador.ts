export interface Jugador {
    id?: number;
    nombres: string;
    apellidos: string;
    numeroCamiseta: number;
    posicion: 'Arquero'|'Defensa'|'Delantero'|'Volante';
    nombreEquipo: string;
}