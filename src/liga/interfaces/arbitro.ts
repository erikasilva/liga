export interface Arbitro {
    id?: number;
    nombres: string;
    apellidos: string;
    tipo: 'Central'|'De Linea';
}