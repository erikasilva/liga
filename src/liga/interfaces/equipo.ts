export interface Equipo {
    id?: number;
    nombrePresidente: string;
    apellidoPresidente: string;
    nombreEquipo: string;
    fechaCreacion: Date;
    colores: String;
}